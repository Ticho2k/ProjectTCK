<?php

    namespace AppBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    
    class ConsultType extends AbstractType {
        public function configureOptions(OptionsResolver $resolver) {
            $resolver->setDefault('data_class', null);
        }

        public function buildForm(FormBuilderInterface $builder, array $options) {
            $builder->add('client', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Client',
                    'class' => 'form-control'
                ),
            ))->add('dokter', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Dokter',
                    'class' => 'form-control'
                ),

            ))->add('datum', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Datum',
                    'class' => 'form-control'
                ),

            ))->add('locatie', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Locatie',
                    'class' => 'form-control'
                ),

            ))->add('kamernummer', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Kamernummer',
                    'class' => 'form-control'
                ),


            ))->add('submit', SubmitType::class , array(
                'label' => 'Aanmaken',
                'attr' => array(
                    'class' => 'btn btn-theme'
                )
            ));
        }

        public function getName() {
            return 'app_consultruimte';
        }

    }

?>