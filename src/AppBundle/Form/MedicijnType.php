<?php

    namespace AppBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;

    
    class MedicijnT
    ype extends AbstractType {
        public function configureOptions(OptionsResolver $resolver) {
            $resolver->setDefault('data_class', null);
        }

        public function buildForm(FormBuilderInterface $builder, array $options) {
            $builder->add('medicijnNaam', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Medicijn',
                    'class' => 'form-control'
                ),
            ))->add('MedicijnBeschrijving', TextType::class , array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Beschrijving',
                    'class' => 'form-control'
                ),
            ));
        }

        public function getName() {
            return 'app_consultruimte';
        }

    }

?>