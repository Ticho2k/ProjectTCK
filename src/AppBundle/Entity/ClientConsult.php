<?php
//
//namespace AppBundle\Entity;
//
//use Doctrine\ORM\Mapping as ORM;
//
///**
// * ClientConsult
// *
// * @ORM\Table(name="client_consult")
// * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientConsultRepository")
// */
//class ClientConsult
//{
//    /**
//     * @var int
//     *
//     * @ORM\Column(name="id", type="integer")
//     * @ORM\Id
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */
//    private $id;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="AanmaakDatum", type="datetimetz")
//     */
//    private $aanmaakDatum;
//
//    /**
//     * @var \DateTime
//     *
//     * @ORM\Column(name="AfspraakDatum", type="datetimetz")
//     */
//    private $afspraakDatum;
//
//
//    /**
//     * Get id
//     *
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * Set aanmaakDatum
//     *
//     * @param \DateTime $aanmaakDatum
//     *
//     * @return ClientConsult
//     */
//    public function setAanmaakDatum($aanmaakDatum)
//    {
//        $this->aanmaakDatum = $aanmaakDatum;
//
//        return $this;
//    }
//
//    /**
//     * Get aanmaakDatum
//     *
//     * @return \DateTime
//     */
//    public function getAanmaakDatum()
//    {
//        return $this->aanmaakDatum;
//    }
//
//    /**
//     * Set afspraakDatum
//     *
//     * @param \DateTime $afspraakDatum
//     *
//     * @return ClientConsult
//     */
//    public function setAfspraakDatum($afspraakDatum)
//    {
//        $this->afspraakDatum = $afspraakDatum;
//
//        return $this;
//    }
//
//    /**
//     * Get afspraakDatum
//     *
//     * @return \DateTime
//     */
//    public function getAfspraakDatum()
//    {
//        return $this->afspraakDatum;
//    }
//}
//
