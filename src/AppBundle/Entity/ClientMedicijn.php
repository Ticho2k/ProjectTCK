<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientMedicijn
 *
 * @ORM\Table(name="client_medicijn")
 * @ORM\Entity
 */
class ClientMedicijn
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="MedicijnNaam", type="string", length=255)
     */
    private $medicijnNaam;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set medicijnNaam
     *
     * @param string $medicijnNaam
     *
     * @return ClientMedicijn
     */
    public function setMedicijnNaam($medicijnNaam)
    {
        $this->medicijnNaam = $medicijnNaam;

        return $this;
    }

    /**
     * Get medicijnNaam
     *
     * @return string
     */
    public function getMedicijnNaam()
    {
        return $this->medicijnNaam;
    }
}

