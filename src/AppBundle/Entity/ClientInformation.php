<?php
//
//namespace AppBundle\Entity;
//
//use Doctrine\ORM\Mapping as ORM;
//
///**
// * ClientInformation
// *
// * @ORM\Table(name="client_information")
// * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientInformationRepository")
// */
//class ClientInformation
//{
//    /**
//     * @var int
//     *
//     * @ORM\Column(name="id", type="integer")
//     * @ORM\Id
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */
//    private $id;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Voornaam", type="string", length=255)
//     */
//    private $voornaam;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Tussenvoegsel", type="string", length=255, nullable=true)
//     */
//    private $tussenvoegsel;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Achternaam", type="string", length=255, nullable=true)
//     */
//    private $achternaam;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Postcode", type="string", length=6)
//     */
//    private $postcode;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Plaats", type="string", length=255)
//     */
//    private $plaats;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Adres", type="string", length=255, nullable=true)
//     */
//    private $adres;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Verzekeraar", type="string", length=255)
//     */
//    private $verzekeraar;
//
//    /**
//     * @var string
//     *
//     * @ORM\Column(name="Dokter", type="string", length=255)
//     */
//    private $dokter;
//
//    /**
//     * @var int
//     *
//     * @ORM\Column(name="PatientNummer", type="integer", unique=true)
//     */
//    private $patientNummer;
//
//
//    /**
//     * Get id
//     *
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * Set voornaam
//     *
//     * @param string $voornaam
//     *
//     * @return ClientInformation
//     */
//    public function setVoornaam($voornaam)
//    {
//        $this->voornaam = $voornaam;
//
//        return $this;
//    }
//
//    /**
//     * Get voornaam
//     *
//     * @return string
//     */
//    public function getVoornaam()
//    {
//        return $this->voornaam;
//    }
//
//    /**
//     * Set tussenvoegsel
//     *
//     * @param string $tussenvoegsel
//     *
//     * @return ClientInformation
//     */
//    public function setTussenvoegsel($tussenvoegsel)
//    {
//        $this->tussenvoegsel = $tussenvoegsel;
//
//        return $this;
//    }
//
//    /**
//     * Get tussenvoegsel
//     *
//     * @return string
//     */
//    public function getTussenvoegsel()
//    {
//        return $this->tussenvoegsel;
//    }
//
//    /**
//     * Set achternaam
//     *
//     * @param string $achternaam
//     *
//     * @return ClientInformation
//     */
//    public function setAchternaam($achternaam)
//    {
//        $this->achternaam = $achternaam;
//
//        return $this;
//    }
//
//    /**
//     * Get achternaam
//     *
//     * @return string
//     */
//    public function getAchternaam()
//    {
//        return $this->achternaam;
//    }
//
//    /**
//     * Set postcode
//     *
//     * @param string $postcode
//     *
//     * @return ClientInformation
//     */
//    public function setPostcode($postcode)
//    {
//        $this->postcode = $postcode;
//
//        return $this;
//    }
//
//    /**
//     * Get postcode
//     *
//     * @return string
//     */
//    public function getPostcode()
//    {
//        return $this->postcode;
//    }
//
//    /**
//     * Set plaats
//     *
//     * @param string $plaats
//     *
//     * @return ClientInformation
//     */
//    public function setPlaats($plaats)
//    {
//        $this->plaats = $plaats;
//
//        return $this;
//    }
//
//    /**
//     * Get plaats
//     *
//     * @return string
//     */
//    public function getPlaats()
//    {
//        return $this->plaats;
//    }
//
//    /**
//     * Set adres
//     *
//     * @param string $adres
//     *
//     * @return ClientInformation
//     */
//    public function setAdres($adres)
//    {
//        $this->adres = $adres;
//
//        return $this;
//    }
//
//    /**
//     * Get adres
//     *
//     * @return string
//     */
//    public function getAdres()
//    {
//        return $this->adres;
//    }
//
//    /**
//     * Set verzekeraar
//     *
//     * @param string $verzekeraar
//     *
//     * @return ClientInformation
//     */
//    public function setVerzekeraar($verzekeraar)
//    {
//        $this->verzekeraar = $verzekeraar;
//
//        return $this;
//    }
//
//    /**
//     * Get verzekeraar
//     *
//     * @return string
//     */
//    public function getVerzekeraar()
//    {
//        return $this->verzekeraar;
//    }
//
//    /**
//     * Set dokter
//     *
//     * @param string $dokter
//     *
//     * @return ClientInformation
//     */
//    public function setDokter($dokter)
//    {
//        $this->dokter = $dokter;
//
//        return $this;
//    }
//
//    /**
//     * Get dokter
//     *
//     * @return string
//     */
//    public function getDokter()
//    {
//        return $this->dokter;
//    }
//
//    /**
//     * Set patientNummer
//     *
//     * @param integer $patientNummer
//     *
//     * @return ClientInformation
//     */
//    public function setPatientNummer($patientNummer)
//    {
//        $this->patientNummer = $patientNummer;
//
//        return $this;
//    }
//
//    /**
//     * Get patientNummer
//     *
//     * @return int
//     */
//    public function getPatientNummer()
//    {
//        return $this->patientNummer;
//    }
//}
//
