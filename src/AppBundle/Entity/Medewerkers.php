<?php
//
//namespace AppBundle\Entity;
//
///**
// * Medewerkers
// */
//class Medewerkers
//{
//    /**
//     * @var int
//     */
//    private $id;
//
//    /**
//     * @var string
//     */
//    private $voornaam;
//
//    /**
//     * @var string
//     */
//    private $tussenvoegsel;
//
//    /**
//     * @var string
//     */
//    private $achternaam;
//
//    /**
//     * @var string
//     */
//    private $functie;
//
//    /**
//     * @var string
//     */
//    private $email;
//
//    /**
//     * @var string
//     */
//    private $telefoonnummer;
//
//
//    /**
//     * Get id
//     *
//     * @return int
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * Set voornaam
//     *
//     * @param string $voornaam
//     *
//     * @return Medewerkers
//     */
//    public function setVoornaam($voornaam)
//    {
//        $this->voornaam = $voornaam;
//
//        return $this;
//    }
//
//    /**
//     * Get voornaam
//     *
//     * @return string
//     */
//    public function getVoornaam()
//    {
//        return $this->voornaam;
//    }
//
//    /**
//     * Set tussenvoegsel
//     *
//     * @param string $tussenvoegsel
//     *
//     * @return Medewerkers
//     */
//    public function setTussenvoegsel($tussenvoegsel)
//    {
//        $this->tussenvoegsel = $tussenvoegsel;
//
//        return $this;
//    }
//
//    /**
//     * Get tussenvoegsel
//     *
//     * @return string
//     */
//    public function getTussenvoegsel()
//    {
//        return $this->tussenvoegsel;
//    }
//
//    /**
//     * Set achternaam
//     *
//     * @param string $achternaam
//     *
//     * @return Medewerkers
//     */
//    public function setAchternaam($achternaam)
//    {
//        $this->achternaam = $achternaam;
//
//        return $this;
//    }
//
//    /**
//     * Get achternaam
//     *
//     * @return string
//     */
//    public function getAchternaam()
//    {
//        return $this->achternaam;
//    }
//
//    /**
//     * Set functie
//     *
//     * @param string $functie
//     *
//     * @return Medewerkers
//     */
//    public function setFunctie($functie)
//    {
//        $this->functie = $functie;
//
//        return $this;
//    }
//
//    /**
//     * Get functie
//     *
//     * @return string
//     */
//    public function getFunctie()
//    {
//        return $this->functie;
//    }
//
//    /**
//     * Set email
//     *
//     * @param string $email
//     *
//     * @return Medewerkers
//     */
//    public function setEmail($email)
//    {
//        $this->email = $email;
//
//        return $this;
//    }
//
//    /**
//     * Get email
//     *
//     * @return string
//     */
//    public function getEmail()
//    {
//        return $this->email;
//    }
//
//    /**
//     * Set telefoonnummer
//     *
//     * @param string $telefoonnummer
//     *
//     * @return Medewerkers
//     */
//    public function setTelefoonnummer($telefoonnummer)
//    {
//        $this->telefoonnummer = $telefoonnummer;
//
//        return $this;
//    }
//
//    /**
//     * Get telefoonnummer
//     *
//     * @return string
//     */
//    public function getTelefoonnummer()
//    {
//        return $this->telefoonnummer;
//    }
//}
//
