<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsultRuimtes
 *
 * @ORM\Entity
 * @ORM\Table(name="consult_ruimtes")
 */
class ConsultRuimtes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Locatie", type="string", length=255)
     */
    private $locatie;

    /**
     * @var string
     *
     * @ORM\Column(name="KamerNummer", type="string", length=255)
     */
    private $kamerNummer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locatie
     *
     * @param string $locatie
     *
     * @return ConsultRuimtes
     */
    public function setLocatie($locatie)
    {
        $this->locatie = $locatie;

        return $this;
    }

    /**
     * Get locatie
     *
     * @return string
     */
    public function getLocatie()
    {
        return $this->locatie;
    }

    /**
     * Set kamerNummer
     *
     * @param string $kamerNummer
     *
     * @return ConsultRuimtes
     */
    public function setKamerNummer($kamerNummer)
    {
        $this->kamerNummer = $kamerNummer;

        return $this;
    }

    /**
     * Get kamerNummer
     *
     * @return string
     */
    public function getKamerNummer()
    {
        return $this->kamerNummer;
    }
}

