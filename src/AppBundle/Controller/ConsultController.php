<?php

namespace AppBundle\Controller;

use AppBundle\Form\ConsultRuimtesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ConsultRuimtes;

class ConsultController extends Controller
{
      public function AddRuimteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $consultRuimtes = new ConsultRuimtes();
        $form = $this->createForm(ConsultRuimtesType::class, $consultRuimtes);
        $form->handleRequest($request);

             if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $Consult = $form->getData();
//                    $Consult->setLocatie($form->get('locatie'));
//                    $Consult->setKamerNummer($form->get('kamernummer'));
                    $em->persist($Consult);
                    $em->flush();
                }
            }

        return $this->render('administrator/client-ruimte.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
