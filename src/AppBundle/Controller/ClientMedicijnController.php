<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ClientMedicijn;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Clientmedicijn controller.
 *
 * @Route("clientmedicijn")
 */
class ClientMedicijnController extends Controller
{
    /**
     * Lists all clientMedicijn entities.
     *
     * @Route("/", name="clientmedicijn_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clientMedicijns = $em->getRepository('AppBundle:ClientMedicijn')->findAll();

        return $this->render('clientmedicijn/index.html.twig', array(
            'clientMedicijns' => $clientMedicijns,
        ));
    }

    /**
     * Creates a new clientMedicijn entity.
     *
     * @Route("/new", name="clientmedicijn_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clientMedicijn = new Clientmedicijn();
        $form = $this->createForm('AppBundle\Form\ClientMedicijnType', $clientMedicijn);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clientMedicijn);
            $em->flush();

            return $this->redirectToRoute('clientmedicijn_show', array('id' => $clientMedicijn->getId()));
        }

        return $this->render('clientmedicijn/new.html.twig', array(
            'clientMedicijn' => $clientMedicijn,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clientMedicijn entity.
     *
     * @Route("/{id}", name="clientmedicijn_show")
     * @Method("GET")
     */
    public function showAction(ClientMedicijn $clientMedicijn)
    {
        $deleteForm = $this->createDeleteForm($clientMedicijn);

        return $this->render('clientmedicijn/show.html.twig', array(
            'clientMedicijn' => $clientMedicijn,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clientMedicijn entity.
     *
     * @Route("/{id}/edit", name="clientmedicijn_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ClientMedicijn $clientMedicijn)
    {
        $deleteForm = $this->createDeleteForm($clientMedicijn);
        $editForm = $this->createForm('AppBundle\Form\ClientMedicijnType', $clientMedicijn);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clientmedicijn_edit', array('id' => $clientMedicijn->getId()));
        }

        return $this->render('clientmedicijn/edit.html.twig', array(
            'clientMedicijn' => $clientMedicijn,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clientMedicijn entity.
     *
     * @Route("/{id}", name="clientmedicijn_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ClientMedicijn $clientMedicijn)
    {
        $form = $this->createDeleteForm($clientMedicijn);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clientMedicijn);
            $em->flush();
        }

        return $this->redirectToRoute('clientmedicijn_index');
    }

    /**
     * Creates a form to delete a clientMedicijn entity.
     *
     * @param ClientMedicijn $clientMedicijn The clientMedicijn entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ClientMedicijn $clientMedicijn)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clientmedicijn_delete', array('id' => $clientMedicijn->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
