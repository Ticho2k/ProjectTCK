<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ConsultRuimtes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Consultruimte controller.
 *
 * @Route("consultruimtes")
 */
class ConsultRuimtesController extends Controller
{
    /**
     * Lists all consultRuimte entities.
     *
     * @Route("/", name="consultruimtes_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $consultRuimtes = $em->getRepository('AppBundle:ConsultRuimtes')->findAll();

        return $this->render('consultruimtes/index.html.twig', array(
            'consultRuimtes' => $consultRuimtes,
        ));
    }

    /**
     * Creates a new consultRuimte entity.
     *
     * @Route("/new", name="consultruimtes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $consultRuimte = new ConsultRuimtes();
        $form = $this->createForm('AppBundle\Form\ConsultRuimtesType', $consultRuimte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($consultRuimte);
            $em->flush();

            return $this->redirectToRoute('consultruimtes_show', array('id' => $consultRuimte->getId()));
        }

        return $this->render('consultruimtes/new.html.twig', array(
            'consultRuimte' => $consultRuimte,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a consultRuimte entity.
     *
     * @Route("/{id}", name="consultruimtes_show")
     * @Method("GET")
     */
    public function showAction(ConsultRuimtes $consultRuimte)
    {
        $deleteForm = $this->createDeleteForm($consultRuimte);

        return $this->render('consultruimtes/show.html.twig', array(
            'consultRuimte' => $consultRuimte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing consultRuimte entity.
     *
     * @Route("/{id}/edit", name="consultruimtes_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ConsultRuimtes $consultRuimte)
    {
        $deleteForm = $this->createDeleteForm($consultRuimte);
        $editForm = $this->createForm('AppBundle\Form\ConsultRuimtesType', $consultRuimte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('consultruimtes_edit', array('id' => $consultRuimte->getId()));
        }

        return $this->render('consultruimtes/edit.html.twig', array(
            'consultRuimte' => $consultRuimte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a consultRuimte entity.
     *
     * @Route("/{id}", name="consultruimtes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ConsultRuimtes $consultRuimte)
    {
        $form = $this->createDeleteForm($consultRuimte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($consultRuimte);
            $em->flush();
        }

        return $this->redirectToRoute('consultruimtes_index');
    }

    /**
     * Creates a form to delete a consultRuimte entity.
     *
     * @param ConsultRuimtes $consultRuimte The consultRuimte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ConsultRuimtes $consultRuimte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consultruimtes_delete', array('id' => $consultRuimte->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
