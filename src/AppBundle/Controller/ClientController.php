<?php

    namespace AppBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;

    class ClientController extends Controller {
        public function clientIndexAction(Request $request) {
            return $this->render('client/client-dashboard.html.twig');
        }

        public function allClientAction(Request $request) {
            return $this->render('administrator/client-overview.html.twig');
        }

        public function editClientAction(Request $request) {
//            edit and create function
            return $this->render('administrator/client-edit.html.twig');
        }

        public function deleteClientAction(Request $request) {
        }

        public function detailClientAction(Request $request) {
//            alle informatie over de client plus consults
            return $this->render('administrator/client-detail.html.twig');
        }

    }
